import './ExpenseItem.css'
import PropTypes from 'prop-types';

export function ExpenseItem({ expense }) {

  return (
    <div className='expense-item'>
      <div>{expense.date}</div>
      <div className='expense-item__description'>
        <h2>{expense.title}</h2>
        <div className='expense-item__price'>$ {expense.amount}</div>
      </div>
    </div>
  )
}

ExpenseItem.propTypes = {
  expense: PropTypes.shape({
    date: PropTypes.string,
    title: PropTypes.string,
    amount: PropTypes.number
  })
};