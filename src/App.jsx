import { ExpenseItem } from './components/Expenses/ExpenseItem'
import './App.css'

function App() {
  const expenses = [
    {
      id: 'e1',
      title: 'New TV',
      amount: 799.49,
      date: new Date(2021,2,12).toISOString()
    },
    {
      id: 'e2',
      title: 'Car Insurance',
      amount: 294.67,
      date: new Date(2021,2,28).toISOString()
    },
    {
      id: 'e3',
      title: 'New Desk (wooden)',
      amount: 450,
      date: new Date(2021,5,12).toISOString()
    }
  ]
  return (
    <>
      <h1>Expenses App</h1>
      <ExpenseItem expense={expenses[0]} />
      <ExpenseItem expense={expenses[1]} />
      <ExpenseItem expense={expenses[2]} />
    </>
  )
}

export default App
